var express = require('express');
var app = express();
var bodyParser = require('body-parser')
const mysql = require('mysql');
const passport = require('passport')
const passportConfig = require('./passport/googleStrategy')
var models = require('./models/user');
const CookieSession = require("cookie-session")
const CookieParser = require('cookie-parser')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(CookieParser('encryptkey'));
app.use(CookieSession({
  name: 'session',
  maxAge: 24 * 60 * 60 * 1000,
  keys: ['encryptkey'],
  httpOnly: true,
}))
app.use(passport.initialize())
app.use(passport.session())

const dotenv = require('dotenv');
dotenv.config();

const db = mysql.createConnection({
  host: process.env.HOST,
  user: "admin",
  password: process.env.PASSWORD,
  database: process.env.DATABASE
})

db.connect((err) => {
  if (err) {
    console.log(process.env.HOST)
    throw err;
  }
  console.log("connected to pgdb...")
})

//auth endpoints

app.get('/auth/google', passport.authenticate('google', { scope: ["email", "profile"] }));

app.get('/auth/google/redirect', passport.authenticate('google', { failureRedirect: 'http://localhost:3000/' }),
  function (req, res, user) {
    console.log("cookies", req.cookies)
    console.log(req.isAuthenticated())
    res.redirect("http://localhost:3000/dashboard");
  }
);

app.get('/auth/success', (req, res) => {
  console.log(req.user)
}
);

app.get("/logout", (req, res) => {
  req.logout();
  res.redirect('http://localhost:3000/')
})

//endpoint for tasklists

app.get('/getTaskLists', (req, res) => {
  let sql = `SELECT * FROM task_lists WHERE user_id = "106221926818577058314"`;
  let query = db.query(sql, (err, results) => {
    if (err) throw err;
    console.log(results);
    res.send(results)
  })
})

app.get('/deleteList', (req, res) => {
  let sql = `DELETE FROM task_lists WHERE id ='${req.query.list_id}'`;
  console.log(sql)
  let query = db.query(sql, (err, results) => {
    if (err) console.log(err);
    console.log(results);
    res.end("success")
  })
  res.end("success")
})

app.post('/addList', (req, res) => {
  console.log(req.body)
  let sql = `INSERT INTO task_lists (id, user_id,name)
      VALUES("${req.body.params.id}","${req.body.params.user_id}","${req.body.params.name}")`;
  console.log(sql)
  let query = db.query(sql, (err, results) => {
    if (err) console.log(err);
    res.end("success")
  })
  res.end("success")
})

//endpoints for tasks

app.get('/getNotDonesTasks', (req, res) => {
  console.log(req.query)
  let sql = `SELECT * FROM tasks WHERE list_id = ${req.query.list_id} AND finished = "false"`;
  let query = db.query(sql, (err, results) => {
    if (err) throw err;
    console.log(results);
    res.send(results)
  })
})

app.get('/getDonesTasks', (req, res) => {
  console.log(req.query)
  let sql = `SELECT * FROM tasks WHERE list_id = ${req.query.list_id} AND finished = "true"`;
  let query = db.query(sql, (err, results) => {
    if (err) throw err;
    console.log(results);
    res.send(results)
  })
})

app.post('/addTask', (req, res) => {
  console.log(req.body)
  let sql = `INSERT INTO tasks (list_id,id,user_id,short_description,long_description,creation_date,limit_date,finished)
      VALUES("${req.body.params.list_id}","${req.body.params.id}","4586974","${req.body.params.short_description}","${req.body.params.long_description}","${req.body.params.creation_date}","${req.body.params.limit_date}","false")`;
  console.log(sql)
  let query = db.query(sql, (err, results) => {
    if (err) console.log(err);
    res.end("success")
  })
  res.end("success")
})

app.get('/deleteTask', (req, res) => {
  console.log(req.body);
  let sql = `DELETE FROM tasks WHERE id ='${req.query.id}'`;
  let query = db.query(sql, (err, results) => {
    if (err) console.log(err);
    console.log(results);
    res.end("success")
  })
  res.end("success")
})

app.post('/updateTaskStatus', (req, res) => {
  console.log(req.body)
  let sql = `UPDATE tasks SET finished = "${req.body.params.isfinished}"
  WHERE id ="${req.body.params.task_id}"`;
  let query = db.query(sql, (err, results) => {
    if (err) console.log(err);
    console.log(results);
    res.end("success")
  })
  res.end("success")
})

app.listen(process.env.PORT, function () {
  console.log('Example app listening on port', process.env.PORT);
});