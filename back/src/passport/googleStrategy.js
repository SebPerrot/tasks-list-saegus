var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const Sequelize = require('sequelize');
const sequelize = new Sequelize("saegusdbtest", "admin", "u2315125o", {
    host: 'saegusdbtest.ctjhxkfn9owu.us-east-2.rds.amazonaws.com',
    dialect: 'mysql',
    logging: false,
});


// const User = require('../models/user')(sequelize, DataTypes);

var User = sequelize.define('user', {

    firstname: Sequelize.STRING,
    mail: Sequelize.STRING,
    lastname: Sequelize.STRING,
    id: { type: Sequelize.STRING, primaryKey: true },

}, {
        timestamps: false
    });

passport.serializeUser((user, done) => {
    // console.log("user is logged as", user)
    done(null, user.id)
})

passport.deserializeUser((id, done) => {
    // console.log(id)
    User.findOne({ where: { id: id } }).then((user) => {
        done(null, user)
    })
})

passport.use('google', new GoogleStrategy({
    clientID: "416641852982-piqipm48jscja0tdl3agktd366b6d7qh.apps.googleusercontent.com",
    clientSecret: "V7d7JubVPOYCZvUWD7j06d59",
    callbackURL: "http://localhost:5000/auth/google/redirect"
},
    function (accessToken, refreshToken, profile, done) {
        User.findOne({
            where: {
                mail: profile.emails[0].value
            }
        }).then(function (user, err) {
            if (err)
                return done(err);

            if (!user)
                User.create({ firstname: profile.name.givenName, mail: profile.emails[0].value, lastname: profile.name.familyName, id: profile.id }).then(newUser => {
                return done(null,newUser)
                })

            if (user) {
                return done(null, user)
            }
        })
    }
));
