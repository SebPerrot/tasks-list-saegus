

"use strict";

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("user", {
      firstname: DataTypes.TEXT,
      mail: DataTypes.TEXT,
      lastname: DataTypes.TEXT,
      id: DataTypes.INTEGER,
    });
  
    return User;
  };