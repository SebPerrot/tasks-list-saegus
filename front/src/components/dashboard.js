import React from 'react'
import axios from 'axios'
import Months from '../utils/Months'
import whitePlusIcon from '../utils/images/plus.png'
import BlackCross from '../utils/images/black_cross.png'
import WhiteCross from '../utils/images/white_cross.png'
import BoyImage from '../utils/images/boy_background.png'
import GirlImage from '../utils/images/girl_background.png'
import LeftArrow from '../utils/images/left_arrow.png'
import DatePicker from 'react-datepicker'
import Poubelle from '../utils/images/poubelle.png'
import PlusWhite from '../utils/images/plus.png'
import DescriptionIcon from '../utils/images/description.png'
import RightArrow from '../utils/images/right_arrow.png'
import Pen from '../utils/images/pen.png'
import CheckIcon from '../utils/images/checked.png'
import "react-datepicker/dist/react-datepicker.css"
import Logo from '../utils/images/black_logo.png'


class dashboard extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            user_id: null,

            tasksLists: [],
            tasks: [],
            doneTasks: [],

            //addlist
            addList_name: "",
            words: ['le chat', 'demain', 'jardin', 'anniversaires'],

            //selectList
            selectedList: null,

            // deletelist
            selectedList_delete: null,

            //addTask
            short_description: "",
            long_description: "",
            limit_date: null,
            tomorrow: null,

            // modals
            showAddListModal: false,
            showDeleteListModal: false,
            showAddTaskModal: false,
            showDonesTasks: false,

            // selectTask
            selectedTask: null,
            selectedTask_limit_date: null,
            selectedTask_creation_date: null,
            selectedTask_isDone: null,
            selectedTask_index: null,
        }
        this.deleteTaskList = this.deleteTaskList.bind(this)
        this.addList = this.addList.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.addTask = this.addTask.bind(this)
        this.deleteTask = this.deleteTask.bind(this)
        this.selectTask = this.selectTask.bind(this)
        this.changeTaskStatus = this.changeTaskStatus.bind(this)
        this.moovePannelLeft = this.moovePannelLeft.bind(this)
    }

    componentDidMount() {

        axios.get('http://localhost:5000/getTaskLists', { params: { id: "106221926818577058314" } }).then(res => {
            this.setState({ tasksLists: res.data }, () => {
                if (this.state.tasksLists.length > 0) {
                    this.setState({ selectedList: 0, selectedList_delete: 0 })
                }
            })
        })


        var today = new Date();
        var tomorrow = new Date();
        tomorrow.setDate(today.getDate() + 1);
        this.setState({ tomorrow: tomorrow, limit_date: tomorrow })
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value })
    }

    deleteTaskList(list_index) {
        axios.get('http://localhost:5000/deleteList', { params: { list_id: this.state.tasksLists[list_index].id } }).then((res) => {
            this.state.tasksLists.splice(list_index, 1);
            this.setState({ selectedList: null, selectedTask: null, tasks: [], doneTasks: [], selectedList_delete: 0, selectedTask: null, selectedTask_index: null, selectedTask_isDone: null, selectedTask_limit_date: null })
            this.forceUpdate()
        })
    }

    addList(list_index) {
        let id = Math.floor(100000 + Math.random() * 900000);
        axios.post('http://localhost:5000/addList', { params: { id: id, user_id: "106221926818577058314", name: this.state.addList_name } }).then((res) => {
            let newList = { name: this.state.addList_name, id: id, user_id: "4586974" }
            this.state.tasksLists.push(newList);
            this.setState({ showAddListModal: false, selectedList: this.state.tasksLists.length - 1 })
            this.forceUpdate()
        })
    }

    selectList(list_index) {
        axios.get('http://localhost:5000/getNotDonesTasks', { params: { list_id: this.state.tasksLists[list_index].id } }).then((res) => {
            this.setState({ tasks: res.data, selectedList: list_index, selectedTask: null, selectedList_delete: list_index })
        })
        axios.get('http://localhost:5000/getDonesTasks', { params: { list_id: this.state.tasksLists[list_index].id } }).then((res) => {
            this.setState({ doneTasks: res.data, })
        })
    }

    addTask() {
        let id = Math.floor(100000 + Math.random() * 900000);
        axios.post('http://localhost:5000/addTask', { params: { list_id: this.state.tasksLists[this.state.selectedList].id, id: id, user_id: "4586974", long_description: this.state.long_description, short_description: this.state.short_description, limit_date: Number(this.state.limit_date.getTime().toString().substring(0, 8) + '00000'), creation_date: new Date().getTime() } }).then((res) => {
            let newTask = { list_id: this.state.tasksLists[this.state.selectedList].id, id: id, user_id: "4586974", long_description: this.state.long_description, short_description: this.state.short_description, limit_date: Number(this.state.limit_date.getTime().toString().substring(0, 8) + '00000'), creation_date: new Date().getTime(), finished: "false" }
            this.state.tasks.push(newTask);
            this.setState({ showAddTaskModal: false })
            this.forceUpdate()
        })
    }

    deleteTask(task_id, task_index, done) {
        console.log(task_id, task_index, done)
        axios.get('http://localhost:5000/deleteTask', { params: { id: task_id } }).then((res) => {
            if (done === "false") {
                this.state.tasks.splice(task_index, 1);
            } else {
                this.state.doneTasks.splice(task_index, 1);
            }
            this.setState({ selectedTask: null, selectedTask_index: null, selectedTask_isDone: null, selectedTask_limit_date: null })
            this.forceUpdate()
        })
    }

    selectTask() {
        let limit_date = new Date(this.state.selectedTask.limit_date).toLocaleDateString("en-GB").split('/');
        let limit_dateFormat = limit_date[0] + " " + Months[limit_date[1]] + " " + limit_date[2];
        let creation_date = new Date(this.state.selectedTask.creation_date).toLocaleDateString("en-GB").split('/');
        let creation_dateFormat = creation_date[0] + " " + Months[creation_date[1]] + " " + creation_date[2];
        this.setState({ selectedTask_limit_date: limit_dateFormat, selectedTask_creation_date: creation_dateFormat }, () => {

            document.getElementById('right_pannel_container').style.transform = "translate(0,0)"
        })
    }

    changeTaskStatus(task_id, isFinished) {

        if (isFinished === "true") {
            var isTaskFinished = "false"
        } else {
            var isTaskFinished = "true"
        }
        axios.post('http://localhost:5000/updateTaskStatus', { params: { task_id: task_id, isfinished: isTaskFinished } }).then((res) => {
            let newTask = this.state.selectedTask
            if (newTask.finished === "false") {
                newTask['finished'] = "true"
                this.state.doneTasks.push(newTask)
                this.state.tasks.splice(this.state.selectedTask_index, 1)
                this.setState({ selectedTask: null, selectedTask_index: null, selectedTask_isDone: null, selectedTask_limit_date: null })
                this.forceUpdate()
            } else {
                newTask['finished'] = "false"
                this.state.tasks.push(newTask)
                this.state.doneTasks.splice(this.state.selectedTask_index, 1)
                this.setState({ selectedTask: null, selectedTask_index: null, selectedTask_isDone: null, selectedTask_limit_date: null })
                this.forceUpdate()
            }
        })
    }

    moovePannelLeft() {
        document.getElementById('left_pannel').style.transform = "translate(-100%,0)"
    }

    moovePannelRight() {
        document.getElementById('left_pannel').style.transform = "translate(0,0)"
    }

    render() {
        return (
            <div>
                <div className="global_container">
                    <img src={Logo} className="dashboard_logo" />
                    <a href="http://localhost:5000/logout">logout</a>
                    <div className="body_grey_background">
                        <img src={GirlImage} className="body_backround_girl" />
                        <img src={BoyImage} className="body_background_boy" />
                    </div>
                    {/* left pannel */}

                    <div className="dashboard_left_pannel" id="left_pannel">
                        <div className="left_pannel_grey_top_bar"><div className="left_pannel_title">LISTES DES TACHES</div> <img src={LeftArrow} className="left_pannel_moove_pannel_arrow_left" onClick={() => { this.moovePannelLeft() }} /></div>
                        {this.state.tasksLists.length > 0 &&
                            <div>
                                {this.state.tasksLists.map((list, index) => (
                                    <button className="left_pannel_list_container" onClick={() => { this.selectList(index) }} selected="">
                                        <div className="left_pannel_list_name">{list.name}</div>
                                        <img src={BlackCross} className="left_pannel_delete_list_button" onClick={() => { this.setState({ showDeleteListModal: true, selectedList_delete: index }) }} />
                                        <img src={WhiteCross} className="left_pannel_delete_list_button_white" onClick={() => { this.setState({ showDeleteListModal: true, selectedList_delete: index }) }} />
                                    </button>
                                ))}
                            </div>
                        }
                        {this.state.tasksLists.length === 0 && <div className="left_pannel_no_list_text">Aucune liste de tâche n'a été créée</div>}
                        <button className="left_pannel_add_list_button" onClick={() => { this.setState({ showAddListModal: true }) }}>+ AJOUTER UNE LISTE DE TACHES</button>
                    </div>

                    {this.state.showAddListModal === true &&
                        <div className="add_list_modal_background">
                            <div className="add_list_modal_container">
                                <div className="add_list_modal_title">AJOUTER UNE LISTE</div>
                                <div className="add_list_input_container" id="add_list_modal_name"><input onChange={this.handleChange} className="add_list_modal_input" name="addList_name" value={this.state.addList_name} placeholder={this.state.words[this.state.words.length * Math.random() << 0]} /></div>
                                <div className="add_list_buttons_container">
                                    <button className="add_list_modal_button" onClick={() => { this.addList() }} disabled={this.state.addList_name.length === 0}>Créer la liste</button>
                                    <button className="add_list_modal_button" onClick={() => { this.setState({ showAddListModal: false }) }}>annuler</button>
                                </div>
                            </div>
                        </div>
                    }
                    {this.state.showDeleteListModal === true &&
                        <div className="delete_list_modal_background">
                            <div className="delete_list_modal_container">
                                <div className="delete_list_modal_title">VOULEZ VOUS VRAIMENT SUPPRIMER CETTE LISTE ?</div>
                                <div className="delete_list_modal_text">toutes les taches associées à la liste "{this.state.tasksLists[this.state.selectedList_delete].name}" seront supprimées...</div>
                                <div className="delete_list_modal_buttons_container">
                                    <button className="delete_list_modal_button" onClick={() => { this.setState({ showDeleteListModal: false }) }}>annuler</button>
                                    <button className="delete_list_modal_button" onClick={() => { this.deleteTaskList(this.state.selectedList_delete); this.setState({ showDeleteListModal: false }) }}>supprimer</button>
                                </div>
                            </div>
                        </div>
                    }
                    <div className="left_little_container">
                        <img src={RightArrow} className="left_little_pannel_icon_right_arrow" onClick={() => { this.moovePannelRight() }} />
                        <img src={PlusWhite} className="left_little_pannel_icon_plus_white" onClick={() => { this.setState({ showAddListModal: true }) }} />
                        <img src={Poubelle} className="left_little_pannel_icon_poubelle" onClick={() => { if (this.state.tasksLists.length > 0) { this.setState({ showDeleteListModal: true }) } else { alert('aucune liste à supprimer') } }} />
                    </div>



                    {/* center pannel */}
                    <div className="center_pannel_container">
                        <div className="center_pannel_grey_top_bar">

                            {this.state.selectedList !== null &&
                                <div>
                                    <p className="center_pannel_list_name">{this.state.tasksLists[this.state.selectedList].name.toUpperCase()}</p>
                                    < button className="center_pannel_add_task_button" onClick={() => { this.setState({ showAddTaskModal: true }) }}><img src={whitePlusIcon} className="center_pannel_add_task_plus_icon" />nouvelle tache</button>
                                </div>
                            }
                        </div>

                        {this.state.doneTasks.length === 0 && this.state.tasks.length === 0 && this.state.selectedList === null &&
                            <div className="center_pannel_no_list_selected_text">Sélectionnez une liste de tâches pour commencer</div>
                        }
                        {this.state.tasks.length > 0 &&
                            <div>
                                {this.state.tasks.map((task, index) => (
                                    <div className="center_pannel_task_container" onClick={() => { this.setState({ selectedTask: this.state.tasks[index], selectedTask_isDone: "false", selectedTask_index: index }, () => { this.selectTask() }) }}>
                                        <div className="center_pannel_not_done_task_circle"></div>
                                        <div className="center_pannel_task_name" >{task.short_description}</div>
                                        <img src={BlackCross} className="center_pannel_delete_task_cross" onClick={() => { this.deleteTask(this.state.tasks[index].id, index, "false") }} />
                                    </div>
                                ))}
                            </div>
                        }
                        {this.state.doneTasks.length > 0 &&
                            <div>
                                {this.state.showDonesTasks === false && <div className="center_pannel_done_task_button" onClick={() => { this.setState({ showDonesTasks: true }) }}>{this.state.doneTasks.length} tâches terminées</div>}
                                {this.state.showDonesTasks === true && <div className="center_pannel_done_task_button" onClick={() => { this.setState({ showDonesTasks: false }) }}>cacher les tâches terminées</div>}
                                {this.state.showDonesTasks === true &&
                                    <div>
                                        {this.state.doneTasks.map((task, index) => (
                                            <div className="center_pannel_task_container" onClick={() => { this.setState({ selectedTask: this.state.doneTasks[index], selectedTask_isDone: "true", selectedTask_index: index }, () => { this.selectTask() }) }}>
                                                <div className="center_pannel_done_task_circle"></div>
                                                <div className="center_pannel_task_name">{task.short_description}</div>
                                                <img src={BlackCross} className="center_pannel_delete_task_cross" onClick={() => { this.deleteTask(this.state.doneTasks[index].id, index, "true") }} />
                                            </div>
                                        ))}
                                    </div>
                                }

                            </div>
                        }
                        {this.state.doneTasks.length === 0 && this.state.tasks.length === 0 && this.state.selectedList !== null &&
                            <div className="center_pannel_no_task_selected_elements_container">
                                <div className="center_pannel_no_task_selected_text">Aucune tâche dans cette liste</div>
                                <button className="center_pannel_no_task_selected_button" onClick={() => { this.setState({ showAddTaskModal: true }) }}><img src={whitePlusIcon} className="center_pannel_add_task_plus_icon" />nouvelle tache</button>
                            </div>
                        }

                    </div>
                    {this.state.limit_date && this.state.showAddTaskModal === true &&
                        <div className="add_task_modal_background">
                            <div className="add_task_modal_container">
                                <div className="add_task_modal_title">Ajouter une tâche</div>
                                <div id="add_task_modal_name" className="add_task_modal_input_container"><input className="add_task_modal_input" name="short_description" value={this.state.short_description} onChange={this.handleChange} /></div>
                                <div id="add_task_modal_description" className="add_task_modal_input_container"><input className="add_task_modal_input" name="long_description" value={this.state.long_description} onChange={this.handleChange} /></div>
                                <div id="add_task_modal_date" className="add_task_modal_input_container">
                                    <DatePicker
                                        className="add_task_modal_input"
                                        selected={this.state.limit_date}
                                        minDate={this.state.tomorrow}
                                        dateFormat="d MMMM yyyy "
                                        onChange={(date) => { this.setState({ limit_date: date }) }}
                                    /></div>
                                <div className="add_task_modal_buttons_container">
                                    <button className="add_task_modal_buttons" onClick={() => { this.addTask() }} disabled={this.state.short_description.length === 0}>Créer</button>
                                    <button className="add_task_modal_buttons" onClick={() => { this.setState({ showAddTaskModal: false }) }}>Annuler</button>
                                </div>
                            </div>
                        </div>
                    }


                    {/* right pannel */}
                    {!this.state.selectedTask &&
                        <div className="right_pannel_no_task_selected_text">selectionnez ou créez une tâche pour voir les détails de celle-ci</div>
                    }
                    {this.state.selectedTask &&
                        <div className="right_pannel_container" id="right_pannel_container">
                            <div className="right_pannel_colored_top_bar"></div>
                            <div className="right_pannel_title">{this.state.selectedTask.short_description.toUpperCase()}</div>
                            {this.state.selectedTask_limit_date && <div className="right_pannel_text">{this.state.selectedTask_limit_date}</div>}
                            <div className="right_pannel_text"><img src={DescriptionIcon} className="right_pannel_description_icon" />{this.state.selectedTask.long_description}{this.state.selectedTask.long_description.length === 0 && "Aucune description"}</div>
                            {this.state.selectedTask_creation_date && <div className="right_pannel_text"><img src={Pen} className="right_pannel_pen_icon" />Créée le {this.state.selectedTask_creation_date}</div>}
                            {this.state.selectedTask.finished === "true" &&
                                <div className="right_pannel_text"><img src={CheckIcon} className="right_pannel_pen_icon" />tache terminée</div>
                            }
                            {this.state.selectedTask.finished === "false" &&
                                <div className="right_pannel_text"><img src={CheckIcon} className="right_pannel_check_icon" />tache incomplète</div>
                            }
                            <div className="right_pannel_buttons_container">
                                <button className="right_pannel_button" onClick={() => { this.changeTaskStatus(this.state.selectedTask.id, this.state.selectedTask.finished) }}>mettre dans les taches {this.state.selectedTask.finished === "true" && "incomplètes"}{this.state.selectedTask.finished === "false" && "terminées"}</button>
                                <button className="right_pannel_button" onClick={() => { this.deleteTask(this.state.selectedTask.id, this.state.selectedTask_index, this.state.selectedTask_isDone) }}>Supprimer la tache</button>
                            </div>
                        </div>
                    }
                </div >
            </div>
        )
    }
}

export default dashboard