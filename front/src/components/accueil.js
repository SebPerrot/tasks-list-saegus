import React from 'react'
import axios from 'axios'
import PhoneImage from '../utils/images/home_phone.png'
import WhiteLogo from '../utils/images/white_logo.png'

class accueil extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        axios.get('http://localhost:5000/getUsers').then(res => {
            this.setState({ users: res.data })
        })
    }

    render() {
        return (
            <div className="accueil_global_container">
                <img src={WhiteLogo} className="accueil_white_logo" />
                <div className="accueil_informations_container">
                    <div className="accueil_informations_title">UNE NOUVELLE FACON DE GERER VOS TACHES</div>
                    <div className="accueil_informations_text">Planifiez, organisez, gére. Tout en un endroit visuel et collaboratif !</div>
                    <a href="http://localhost:5000/auth/google" className="accueil_google_button">Connexion avec google</a>
                </div>
                <img src={PhoneImage} className="accueil_phone_image" />
            </div>
        )
    }
}

export default accueil;