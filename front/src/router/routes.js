import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Dashboard from '../components/dashboard'
import Accueil from '../components/accueil'


const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Switch>
                <Route exact path="/" component={Accueil} />
                <Route exact path="/dashboard" component={Dashboard} />
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;