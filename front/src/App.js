import React from 'react';
import AppRouter from './router/routes'

function App() {
  return (
    <AppRouter/>
  );
}

export default App;
